'use strict';
const serviceName = "monitor_aggregates";
const serviceConfig = require("./lib/setup.js")(serviceName);
const logger = serviceConfig.logger;
const os = require('os');
const geohash = require('ngeohash');

const Consumer = require("async-kafka").Consumer;
const consumer = new Consumer({
    kafkaBrokers: process.env.KAFKA_BROKERS.split(","), //Array of kafka brokers
    schemaRegistryUrls: null,             //Array of schema registry URLs
    groupId: serviceName ,                       //If not specified a random GUID wil be used and commits will not function
    clientId: serviceName + "1",                         //If not specified a random GUID wil be used
    maxQueueSize: 2000,                                 //Maximum number of messages to keep in async queue
    statsIntervalSec: 20,                                //Interval between stats calculation
    maxFetchSize: 1000,                                  //Maximum number of messages to fetch in a batch. Processing of batch is syncrounous and influences flow.
    commitIntervalMs: 1000,                              //Interval to commit offsets if topic has 'autoCommit' set
    maxEventLoopBlockMs: 10,                            //Maximum time in ms that batch processing will be allowed to block event loop.
    maxWaitMs: 10,                                      //Maximum time to wait to fill a batch of messages. Low number keeps latency low at the cost of IO
    logStats: false,                                      //Log statistics to provided logger
    automaticGracefulShudown: false
}, logger);

const Producer = require("async-kafka").Producer;
const producer = new Producer(
    {
        clientId: serviceName + "1",
        kafkaBrokers: process.env.KAFKA_BROKERS.split(","),
        //schemaRegistryUrl: schemaRegistryUrls,
        maxMessagesInFlight: 1000,
        minProduceLatencyMs: 1,
        statsIntervalSec: 30,
        compressionCodec: "none",
        automaticGracefulShudown: false,
        librdkafkaDebug: false
    }, logger);

const StateManager = require("@zacken/async_state_manager");
const vehicleMasterState = new StateManager({
    stateStoreType: "kafka",
    autoConnect: false,
    changeHandler: async (key, record) => {
        //updateIdLookup(key,record);
        //vehicleMasterHandler(key, record);
    },
    //persistTimeoutMs: 10000,
    persistState: true,
    processRecordsAfterLoaded: false,
    //cleanerIntervalMs: 20000,
    //processRecordsAfterLoaded: false,
    kafka: {
        autoConnect: false,
        consumer: consumer,
        producer: producer,
        topic: process.env.VEHICLE_AGGREGATE_TOPIC
    }
});

const vehicleTimeouts = new Map();
const RELOAD = process.env.RELOAD || false;

async function main() {



    await consumer.connect();
    await producer.connect();
    await vehicleMasterState.connect();

    let reload = false;
    if (process.env?.RELOAD === "true" || process.env?.RELOAD === "TRUE" || process.env?.RELOAD === "True" || process.env?.RELOAD === 1)
        reload = true;


    let topicConsumerMode = Consumer.TopicConsumeMode.LAST_COMMIT;
    if (reload) {
        topicConsumerMode = Consumer.TopicConsumeMode.EARLIEST;
        await vehicleMasterState.clear();
    }



    await consumer.subscribe([{
        topic: process.env.KAFKA_VEHICLE_KEY,                           //Name of topic on broker
        decoderType: Consumer.DecoderType.JSON,             //Can be of type AUTO, JSON, AVRO, STRING and BUFFER
        consumeMode: topicConsumerMode, //Available modes EARLIEST, LAST_COMMIT, LATEST and NEXT
        autoCommit: true,                                   //If TRUE topic will be commitet minimum by the intervall specified in consumer property 'commitIntervalMs' and to latest processed message on unassign
        keepOffsetOnReassign: true,                        //If TRUE consumer will continue from after last processed message if partition is reassigned even if not commited
        processor: async (message, kafkaHeader) => {
            //await processVehiclePosition(kafkaHeader.key, message);
            if (!message) vehicleMasterState.delete(kafkaHeader.key);
            else {
                let vehicleIcon = "QUESTION_MARK";
                if (message?.type) vehicleIcon = message.type;

                let operatorName = null;
                if (message?.operatorRef) operatorName = message.operatorRef.split(":")[2];

                let dotColor = "#AFAFAF";
                if (message?.type === "BUS") {
                    if (message?.livery === "REGIONAL_BUS") dotColor = "#76A300";
                    else if (message?.livery === "CITY_BUS") dotColor = "#E60000";
                }
                else if (message?.type === "TRAM") dotColor = "#0B91EF";
                else if (message?.type === "FERRY") dotColor = "#682C88";

                const result = {
                    push: {
                        vehicleRef: message.key,
                        vehicleIcon: vehicleIcon,
                        livery: message.livery || null,
                        dotColor: dotColor,
                        operatorName: operatorName,
                        localVehicleId: message.localVehicleId || null,
                        sisId: message.sisId || null,
                        nobinaId: message.nobinaId || null,
                        vixId: message.vixId || null,
                        vin: message.key || null,
                        registrationNumber: message.registrationNumber || null,
                        engineType: message.engineType || null,
                        contractAreaName: message.contractArea || null,
                        homeDepotName: message.homeDepot || null,
                        operatorCode: message.operatorCode || null,
                        hasLowFloorAccess: message.hasLowFloorAccess || null,
                        operatorRef: message.operatorRef || null,
                        babyCarriageCapacity: message.babyCarriageCapacity || null,
                        doorConfiguration: message.doorConfiguration || null,
                        numberOfDoors: message.numberOfDoors || null,
                        totalPassengerCapacity: message.totalPassengerCapacity || null,
                        seatedPassengerCapacity: message.seatedPassengerCapacity || null,
                        tipupSeatPassengerCapacity: message.tipupSeatPassengerCapacity || null,
                        wheelcairCapacity: message.wheelcairCapacity || null,
                        length: message.length || null,
                        height: message.height || null,
                    },
                    pull: {
                        digitalFeatures: message.digitalFeatures || null
                    }
                };

                vehicleMasterState.merge(message.key, result);
            }

        }
    }]);

    await consumer.subscribe([{
        topic: process.env.KAFKA_VEHICLE_POSITION,                           //Name of topic on broker
        decoderType: Consumer.DecoderType.JSON,             //Can be of type AUTO, JSON, AVRO, STRING and BUFFER
        consumeMode: topicConsumerMode, //Available modes EARLIEST, LAST_COMMIT, LATEST and NEXT
        autoCommit: true,                                   //If TRUE topic will be commitet minimum by the intervall specified in consumer property 'commitIntervalMs' and to latest processed message on unassign
        keepOffsetOnReassign: true,                        //If TRUE consumer will continue from after last processed message if partition is reassigned even if not commited
        processor: async (message, kafkaHeader) => {
            //await processVehiclePosition(kafkaHeader.key, message);
            if (!message) {
                logger.warn("Vehicle is deleting position: " + kafkaHeader.key);
            }
            else {
                if (await vehicleMasterState.has(kafkaHeader.key)) {
                    let isHidden = 0;
                    let gnssStatus = "UNKNOWN";
                    if (message?.gnssStatus) gnssStatus = message.gnssStatus;

                    if (gnssStatus === "OFFLINE") isHidden = 1;
                    else if (!message?.positionTimestamp) isHidden = 1;
                    else {
                        const timeDiff = (new Date()) - (new Date(message?.positionTimestamp || 0));
                        if (timeDiff > 60000 && message?.qualityIndicators?.source !== "SIS") isHidden = 1;
                        else if (timeDiff > (60000 * 30)) isHidden = 1;
                    }

                    if (message?.longitude && message?.latitude) {
                        const result = {
                            push: {
                                position: geohash.encode(message.latitude, message.longitude, 11),
                                gnssStatus: gnssStatus,
                                gnssSource: message?.qualityIndicators?.source,
                                isHidden: isHidden
                            },
                            pull: {
                                lastPositionTimestamp: message?.positionTimestamp || null
                            }
                        };
                        //if (isHidden === 0) result.push.isHidden = 0;
                        vehicleMasterState.merge(kafkaHeader.key, result);
                    }
                } else logger.warn("Undefined vehicle is sending positions: " + kafkaHeader.key);
            }
        }
    }, {
        topic: process.env.KAFKA_VEHICLE_SENSORS,                           //Name of topic on broker
        decoderType: Consumer.DecoderType.JSON,             //Can be of type AUTO, JSON, AVRO, STRING and BUFFER
        consumeMode: topicConsumerMode, //Available modes EARLIEST, LAST_COMMIT, LATEST and NEXT
        autoCommit: true,                                   //If TRUE topic will be commitet minimum by the intervall specified in consumer property 'commitIntervalMs' and to latest processed message on unassign
        keepOffsetOnReassign: true,                        //If TRUE consumer will continue from after last processed message if partition is reassigned even if not commited

        processor: async (message, kafkaHeader) => {
            if (await vehicleMasterState.has(kafkaHeader.key)) {
                if (!message) {
                    logger.warn("Vehicle is deleting sensors: " + kafkaHeader.key);
                    const currentRecord = await vehicleMasterState.get(kafkaHeader.key);
                    currentRecord.doorsOpen = null;
                    currentRecord.stopSignal = null;
                    currentRecord.externalDisplayCode = null;
                    currentRecord.externalDisplayText = null;
                    currentRecord.externalDisplaySubText = null;
                    currentRecord.externalTemperature = null;
                    currentRecord.cabinTemperature = null;
                    currentRecord.evStateOfCharge = null;
                    currentRecord.evCharging = null;
                    currentRecord.blockRef = null;
                    currentRecord.assignmentId = null;
                    currentRecord.assignmentSource = null;
                    currentRecord.journeyRef = null;
                    currentRecord.journeyId = null;
                    currentRecord.journeyName = null;
                    currentRecord.journeySource = null;
                    currentRecord.isDeadRun = null;
                    currentRecord.externalDisplaySource = null;
                    await vehicleMasterState.set(kafkaHeader.key, currentRecord);
                }
                else {

                    if (message.vehicleRef) {

                        let ringColor = null;

                        let externalDisplayCode = null;
                        let externalDisplayText = null;
                        let externalDisplaySubText = null;
                        if (message?.external_display?.value?.text) {
                            externalDisplayText = message.external_display.value.text;
                            if (message?.external_display?.value?.publicCode) externalDisplayCode = message.external_display.value.publicCode;
                            if (message?.external_display?.value?.subText) externalDisplaySubText = message.external_display.value.subText;
                        }

                        const blockRef = message?.current_block?.value?.currentBlockRef || null;
                        const assignmentSource = message?.current_block?.source || null;
                        const assignmentId = message?.current_block?.value?.vehicleAssignmentID || null;

                        const journeyRef = message?.current_journey?.value?.currentJourneyRef || null;
                        const journeyId = message?.current_journey?.value?.journeyId || null;
                        const journeySource = message?.current_journey?.source || null;
                        const journeyName = message?.current_journey?.value?.journeyName || null;
                        const isDeadRun = message?.current_journey?.value?.isDeadRun || null;

                        let stopSignal = null;
                        if (message?.stop_signal?.value === "ON") {
                            stopSignal = 1;
                            //ringColor = "#FF0000FF";
                        } else if (message?.stop_signal?.value === "OFF") stopSignal = 0;

                        let doorsOpen = null;
                        if (message?.doors_open?.value === "OPEN") {
                            doorsOpen = 1;
                            //ringColor = "#0000FF";
                        }
                        else if (message?.doors_open?.value === "CLOSED") doorsOpen = 0;

                        let cabinTemperature = null;
                        if (message?.cabin_temperature?.front)
                            cabinTemperature = message?.cabin_temperature?.front?.value || null;
                        else if (message?.cabin_temperature)
                            cabinTemperature = message?.cabin_temperature?.value || null;
                        if (cabinTemperature)
                            cabinTemperature = Math.round(cabinTemperature);


                        let externalTemperature = message?.external_temperature?.value || null;
                        if (externalTemperature)
                            externalTemperature = Math.round(externalTemperature);

                        const result = {
                            push: {
                                doorsOpen: doorsOpen,
                                stopSignal: stopSignal,
                                externalDisplayCode: externalDisplayCode,
                                externalDisplayText: externalDisplayText,
                                externalDisplaySubText: externalDisplaySubText,
                                externalTemperature: externalTemperature,
                                cabinTemperature: cabinTemperature,
                                evStateOfCharge: message?.ev_state_of_charge?.value || null,
                                evCharging: message?.ev_charging?.value || null,
                                blockRef: blockRef,
                                assignmentId: assignmentId,
                                assignmentSource: assignmentSource,
                                journeyRef: journeyRef,
                                journeyId: journeyId,
                                journeyName: journeyName,
                                journeySource: journeySource,
                                isDeadRun: isDeadRun,
                                externalDisplaySource: message?.external_display?.source || null,
                            }
                        };
                        vehicleMasterState.merge(kafkaHeader.key, result);
                    }
                }
            } else logger.warn("Undefined vehicle is sending sensors: " + kafkaHeader.key);
        }
    }]);


    setInterval(() => {
        if (vehicleMasterState?.state)
            vehicleMasterState.state.forEach((vehicle) => {
                if (vehicle?.pull?.lastPositionTimestamp) {
                    const now = new Date();
                    const then = new Date(vehicle.pull.lastPositionTimestamp);
                    const diff = now - then;
                    if (diff > (30 * 60000))
                        vehicleMasterState.merge(vehicle.push.vehicleRef, { push: { isHidden: 1 } });
                } else vehicleMasterState.merge(vehicle.push.vehicleRef, { push: { isHidden: 1 } });

            })
    }, 60000)



    logger.info('Service is running');
}

process.on("unhandledRejection", function (error) {
    logger.error("Caugth unhandled Rejection from process");
    shutdownAndExit(error);
});

process.on("uncaughtException", function (error) {
    logger.error("Caugth unhandled Exception from process");
    shutdownAndExit(error);
});

process.on('SIGINT', function () {
    logger.info("Recieved SIGINT from process");
    shutdownAndExit();
});

async function shutdownAndExit(error) {
    let exitCode = 0;
    if (error) {
        logger.error(error);
        exitCode = 1;
    }
    else try {
        // Do gracefull shutdown of components
        console.log("GFSD Disconnecting state");
        await vehicleMasterState.disconnect();
        console.log("GFSD Unsubscribe consumer");
        await consumer.unsubscribe();
        console.log("GFSD Disconnecting producer");
        await producer.disconnect();
        console.log("GFSD Disconnecting consumer");
        await consumer.disconnect();


    } catch (err) {
        logger.error(err);
        exitCode = 1;
    }
    if (exitCode) logger.error("Gracefull shutdown failed");
    else logger.info("Gracefull shutdown complete");
    logger.info("exitCode: " + exitCode);
    logger.info('Service stopped: ' + serviceConfig.serviceName);
    process.exit(exitCode);
}

logger.info('Starting Service: ' + serviceConfig.serviceName);
main();

