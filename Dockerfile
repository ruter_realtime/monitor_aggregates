FROM node:14-alpine as base

RUN apk --no-cache add \
      bash \
      g++ \
      ca-certificates \
      lz4-dev \
      musl-dev \
      cyrus-sasl-dev \
      openssl-dev \
      make \
      python \
      git 

RUN apk add --no-cache --virtual .build-deps gcc zlib-dev libc-dev bsd-compat-headers py-setuptools bash lz4-libs

WORKDIR /usr/src/app
# COPY . /usr/src/app/
COPY package* /usr/src/app/

RUN npm config set @zacken:registry https://gitlab.com/api/v4/projects/22461149/packages/npm/
RUN npm install
# If you are building your code for production 
# RUN npm ci --only=production

FROM node:14-alpine

RUN apk --no-cache add \
      libsasl \
      lz4-libs

WORKDIR /usr/src/app
COPY --from=base /usr/src/app/node_modules ./node_modules
COPY . .

CMD [ "node","--max-old-space-size=1024","index.js" ]